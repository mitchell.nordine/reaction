use nannou::{prelude::*, wgpu};

use shaderc;

mod consts;
mod sim;

use consts::*;
use sim::*;

// The vertex type that we will use to represent a point on our triangle.
#[repr(C)]
#[derive(Clone, Copy)]
struct Vertex {
    position: [f32; 2],
}

// The vertices that make up the rectangle to which the image will be drawn.
const VERTICES: [Vertex; 4] = [
    Vertex {
        position: [-1.0, -1.0],
    },
    Vertex {
        position: [-1.0, 1.0],
    },
    Vertex {
        position: [1.0, -1.0],
    },
    Vertex {
        position: [1.0, 1.0],
    },
];

impl wgpu::VertexDescriptor for Vertex {
    const STRIDE: wgpu::BufferAddress = std::mem::size_of::<Vertex>() as u64;
    const ATTRIBUTES: &'static [wgpu::VertexAttributeDescriptor] =
        &[wgpu::VertexAttributeDescriptor {
            format: wgpu::VertexFormat::Float2,
            offset: 0,
            shader_location: 0,
        }];
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    let w_id = app.new_window().view(view).build().unwrap();
    let window = app.window(w_id).unwrap();
    let device = window.swap_chain_device();
    let format = Frame::TEXTURE_FORMAT;
    let msaa_samples = window.msaa_samples();

    // Compile shaders
    let (vert_artifact, frag_artifact, comp_artifact) = {
        let mut compiler = shaderc::Compiler::new().unwrap();
        (
            compiler
                .compile_into_spirv(
                    VERT_SHADER_SRC,
                    shaderc::ShaderKind::Vertex,
                    "shader.glsl",
                    "main",
                    None,
                )
                .unwrap(),
            compiler
                .compile_into_spirv(
                    FRAG_SHADER_SRC,
                    shaderc::ShaderKind::Fragment,
                    "shader.glsl",
                    "main",
                    None,
                )
                .unwrap(),
            compiler
                .compile_into_spirv(
                    COMP_SHADER_SRC,
                    shaderc::ShaderKind::Compute,
                    "shader.glsl",
                    "main",
                    None,
                )
                .unwrap(),
        )
    };

    let vs = vert_artifact.as_binary_u8();
    let vs_spirv =
        wgpu::read_spirv(std::io::Cursor::new(&vs[..])).expect("failed to read hard-coded SPIRV");
    let vs_mod = device.create_shader_module(&vs_spirv);
    let fs = frag_artifact.as_binary_u8();
    let fs_spirv =
        wgpu::read_spirv(std::io::Cursor::new(&fs[..])).expect("failed to read hard-coded SPIRV");
    let fs_mod = device.create_shader_module(&fs_spirv);
    let cs = comp_artifact.as_binary_u8();
    let cs_spirv =
        wgpu::read_spirv(std::io::Cursor::new(&cs[..])).expect("failed to read hard-coded SPIRV");
    let cs_mod = device.create_shader_module(&cs_spirv);

    // Initialize the texture
    let texture = wgpu::TextureBuilder::new()
        .size([GRID_WIDTH as u32, GRID_HEIGHT as u32])
        .format(wgpu::TextureFormat::Rg32Float)
        .usage(wgpu::TextureUsage::COPY_DST | wgpu::TextureUsage::SAMPLED)
        .build(window.swap_chain_device());
    let texture_view = texture.view().build();

    // Create the sampler for sampling from the source texture.
    let sampler = wgpu::SamplerBuilder::new().build(device);

    let bind_group_layout = create_bind_group_layout(device, wgpu::ShaderStage::FRAGMENT);
    let bind_group = create_bind_group(device, &bind_group_layout, &texture_view, &sampler);
    let pipeline_layout = create_pipeline_layout(device, &bind_group_layout);
    let render_pipeline = create_render_pipeline(
        device,
        &pipeline_layout,
        &vs_mod,
        &fs_mod,
        format,
        msaa_samples,
    );

    // Create the vertex buffer.
    let vertex_buffer = device
        .create_buffer_mapped(VERTICES.len(), wgpu::BufferUsage::VERTEX)
        .fill_from_slice(&VERTICES[..]);

    let grid_buffer_size =
        (GRID_WIDTH * GRID_HEIGHT * std::mem::size_of::<[f32; 2]>()) as wgpu::BufferAddress;
    let mut grid = Grid::new();
    grid.init_seeds();
    let grid_buffer = device
        .create_buffer_mapped(
            GRID_WIDTH * GRID_HEIGHT,
            wgpu::BufferUsage::STORAGE | wgpu::BufferUsage::COPY_DST | wgpu::BufferUsage::COPY_SRC,
        )
        .fill_from_slice(grid.0.as_slice().clone());

    let compute_bind_group_layout = wgpu::BindGroupLayoutBuilder::new()
        .storage_buffer(wgpu::ShaderStage::COMPUTE, false, false)
        .build(device);
    let compute_bind_group = wgpu::BindGroupBuilder::new()
        .buffer_bytes(&grid_buffer, 0..grid_buffer_size)
        .build(device, &compute_bind_group_layout);

    let compute_pipeline_layout = create_pipeline_layout(device, &compute_bind_group_layout);
    let compute_pipeline = {
        let compute_stage = wgpu::ProgrammableStageDescriptor {
            module: &cs_mod,
            entry_point: "main",
        };
        let desc = wgpu::ComputePipelineDescriptor {
            layout: &compute_pipeline_layout,
            compute_stage,
        };
        device.create_compute_pipeline(&desc)
    };

    Model::new(
        bind_group,
        render_pipeline,
        vertex_buffer,
        compute_bind_group,
        compute_pipeline,
        grid_buffer,
        texture,
    )
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let window = app.main_window();
    let device = window.swap_chain_device();

    if model.iteration % 50 == 0 {
        println!("iteration: {}; fps: {}", model.iteration, app.fps());
    }

    let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor::default());

    {
        let mut cpass = encoder.begin_compute_pass();
        cpass.set_pipeline(&model.compute_pipeline);
        cpass.set_bind_group(0, &model.compute_bind_group, &[]);
        cpass.dispatch(GRID_WIDTH as u32, GRID_HEIGHT as u32, 1);
    }

    // Submit the compute pass to the device's queue.
    window
        .swap_chain_queue()
        .lock()
        .unwrap()
        .submit(&[encoder.finish()]);

    model.iteration += 1;
}

fn view(_app: &App, model: &Model, frame: Frame) {
    // let draw = app.draw();

    // TODO: Is there a way to make this effective even with the shader render
    // pass?
    // draw.background().color(PURPLE);
    // draw.to_frame(app, &frame).unwrap();

    {
        let mut encoder = frame.command_encoder();

        let buffer_copy_view = model.texture.default_buffer_copy_view(&model.grid_buffer);
        let texture_copy_view = model.texture.default_copy_view();
        let extent = model.texture.extent();
        encoder.copy_buffer_to_texture(buffer_copy_view, texture_copy_view, extent);

        let mut render_pass = wgpu::RenderPassBuilder::new()
            .color_attachment(frame.texture_view(), |color| color)
            .begin(&mut encoder);
        render_pass.set_bind_group(0, &model.bind_group, &[]);
        render_pass.set_pipeline(&model.render_pipeline);
        render_pass.set_vertex_buffers(0, &[(&model.vertex_buffer, 0)]);
        let vertex_range = 0..VERTICES.len() as u32;
        let instance_range = 0..1;
        render_pass.draw(vertex_range, instance_range);
    }
}

fn create_bind_group_layout(
    device: &wgpu::Device,
    visibility: wgpu::ShaderStage,
) -> wgpu::BindGroupLayout {
    wgpu::BindGroupLayoutBuilder::new()
        .sampled_texture(
            wgpu::ShaderStage::FRAGMENT,
            false,
            wgpu::TextureViewDimension::D2,
        )
        .sampler(visibility)
        .build(device)
}

fn create_bind_group(
    device: &wgpu::Device,
    layout: &wgpu::BindGroupLayout,
    texture: &wgpu::TextureView,
    sampler: &wgpu::Sampler,
) -> wgpu::BindGroup {
    wgpu::BindGroupBuilder::new()
        .texture_view(texture)
        .sampler(sampler)
        .build(device, layout)
}

fn create_pipeline_layout(
    device: &wgpu::Device,
    bind_group_layout: &wgpu::BindGroupLayout,
) -> wgpu::PipelineLayout {
    let desc = wgpu::PipelineLayoutDescriptor {
        bind_group_layouts: &[&bind_group_layout],
    };
    device.create_pipeline_layout(&desc)
}

fn create_render_pipeline(
    device: &wgpu::Device,
    layout: &wgpu::PipelineLayout,
    vs_mod: &wgpu::ShaderModule,
    fs_mod: &wgpu::ShaderModule,
    dst_format: wgpu::TextureFormat,
    sample_count: u32,
) -> wgpu::RenderPipeline {
    wgpu::RenderPipelineBuilder::from_layout(layout, vs_mod)
        .fragment_shader(fs_mod)
        .color_format(dst_format)
        .add_vertex_buffer::<Vertex>()
        .sample_count(sample_count)
        .primitive_topology(wgpu::PrimitiveTopology::TriangleStrip)
        .build(device)
}
