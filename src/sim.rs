use nannou::prelude::*;

use rand::prelude::*;

use crate::consts::*;

type Index = (usize, usize);

#[derive(Clone)]
pub struct Grid(pub Vec<[f32; 2]>);

impl Grid {
    pub fn new() -> Self {
        let mut grid = Vec::new();
        for _ in 0..GRID_HEIGHT {
            for _ in 0..GRID_WIDTH {
                grid.push([1., 0.]);
            }
        }

        Grid(grid)
    }

    pub fn init_seeds(&mut self) {
        let mut rng = thread_rng();

        // for _ in 0..rng.gen_range(MIN_SEEDS, MAX_SEEDS) {
        for _ in 0..MAX_SEEDS {
            // let width = rng.gen_range(SEED_MIN_WIDTH, SEED_MAX_WIDTH);
            // let height = rng.gen_range(SEED_MIN_HEIGHT, SEED_MAX_HEIGHT);

            let size = rng.gen_range(SEED_MIN_SIZE, SEED_MAX_SIZE);

            let px = rng.gen_range(size, GRID_WIDTH - size);
            let py = rng.gen_range(size, GRID_HEIGHT - size);

            for y in 0..size {
                for x in 0..size {
                    let x = x as i32 - size as i32 / 2;
                    let y = y as i32 - size as i32 / 2;

                    if rng.gen_bool(SEED_PROB_PER_CELL) {
                        self[((px as i32 + x) as usize, (py as i32 + y) as usize)][1] = 1.;
                    }
                }
            }
        }
    }

    pub fn xy_to_i(x: usize, y: usize) -> usize {
        y * GRID_WIDTH + x
    }

    pub fn i_to_xy(i: usize) -> (usize, usize) {
        (i % GRID_WIDTH, i / GRID_WIDTH)
    }

    pub fn as_bytes(&self) -> &[u8] {
        let slice = self.0.as_slice();
        let len = slice.len() * std::mem::size_of::<[f32; 2]>();
        let ptr = slice.as_ptr() as *const u8;
        unsafe { std::slice::from_raw_parts(ptr, len) }
    }
}

impl std::ops::Index<Index> for Grid {
    type Output = [f32; 2];
    fn index(&self, (x, y): Index) -> &Self::Output {
        &self.0[Self::xy_to_i(x, y)]
    }
}

impl std::ops::IndexMut<Index> for Grid {
    fn index_mut(&mut self, (x, y): Index) -> &mut [f32; 2] {
        &mut self.0[Self::xy_to_i(x, y)]
    }
}

pub struct Model {
    pub bind_group: wgpu::BindGroup,
    pub render_pipeline: wgpu::RenderPipeline,
    pub vertex_buffer: wgpu::Buffer,
    pub compute_bind_group: wgpu::BindGroup,
    pub compute_pipeline: wgpu::ComputePipeline,
    pub grid_buffer: wgpu::Buffer,
    pub texture: wgpu::Texture,

    pub iteration: u32,
}

impl Model {
    pub fn new(
        bind_group: wgpu::BindGroup,
        render_pipeline: wgpu::RenderPipeline,
        vertex_buffer: wgpu::Buffer,
        compute_bind_group: wgpu::BindGroup,
        compute_pipeline: wgpu::ComputePipeline,
        grid_buffer: wgpu::Buffer,
        texture: wgpu::Texture,
    ) -> Self {
        Model {
            bind_group,
            render_pipeline,
            vertex_buffer,
            compute_bind_group,
            compute_pipeline,
            grid_buffer,
            texture,
            iteration: 0,
        }
    }
}
