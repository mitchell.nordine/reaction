// pub const GRID_WIDTH: usize = 400;
// pub const GRID_HEIGHT: usize = 400;

pub const GRID_WIDTH: usize = 400;
pub const GRID_HEIGHT: usize = 400;

pub const STEP_PER_ITER: usize = 10;

pub const MIN_SEEDS: usize = 1;
pub const MAX_SEEDS: usize = 10;
pub const SEED_MIN_SIZE: usize = 2;
pub const SEED_MAX_SIZE: usize = 6;
pub const SEED_PROB_PER_CELL: f64 = 0.8;

// pub const SEED_MIN_WIDTH: usize = 2;
// pub const SEED_MIN_HEIGHT: usize = 2;
// pub const SEED_MAX_WIDTH: usize = 50;
// pub const SEED_MAX_HEIGHT: usize = 50;

pub const COMP_SHADER_SRC: &str = r#"
#version 450

layout(local_size_x = 1) in;

layout(set = 0, binding = 0) buffer Buffer {
    vec2[] data;
};

const uint GRID_WIDTH = 400;
const uint GRID_HEIGHT = 400;

float get_chem(uint chem, uint x, uint y) {
    float value = 0.;

    if (x > 0 && x < GRID_WIDTH && y > 0 && y < GRID_HEIGHT) {
        vec2 ab = data[y * GRID_WIDTH + x];

        if (chem == 0) {
            value = ab.r;
        } else if (chem == 1) {
            value = ab.g;
        } else {
            value = 0.;
        }
    }

    return value;
}

float laplace(uint chem, uint x, uint y) {
    float sum = 0.;

    sum += get_chem(chem, x, y) * -1.;

    sum += get_chem(chem, x, y - 1) * 0.2;
    sum += get_chem(chem, x, y + 1) * 0.2;
    sum += get_chem(chem, x - 1, y) * 0.2;
    sum += get_chem(chem, x + 1, y) * 0.2;

    sum += get_chem(chem, x - 1, y - 1) * 0.05;
    sum += get_chem(chem, x + 1, y - 1) * 0.05;
    sum += get_chem(chem, x - 1, y + 1) * 0.05;
    sum += get_chem(chem, x + 1, y + 1) * 0.05;

    return sum;
}

void main() {
    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
    uint i = y * GRID_WIDTH + x;

    float a = data[i].r;
    float b = data[i].g;

    float da = 1.;
    float db = 0.5;

    float f = 0.0545; // standard
    float k = 0.062;

    // a += (da * self.apply_laplace(&Chemical::A, x as i32, y as i32) - a * b.pow(2.)
    //     + f * (1. - a))
    //     * t;

    // b += (db * self.apply_laplace(&Chemical::B, x as i32, y as i32) + a * b.pow(2.)
    //     - (k + f) * b)
    //     * t;

    a += da * laplace(0, x, y) - a * pow(b, 2.) + f * (1. - a);
    b += db * laplace(1, x, y) + a * pow(b, 2.) - (k + f) * b;

    data[i].r = a;
    data[i].g = b;
    // data[i].r = 0.;
    // data[i].g = 0.;
}
"#;

pub const FRAG_SHADER_SRC: &str = r#"
#version 450

layout(location = 0) in vec2 tex_coords;
layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 0) uniform texture2D tex;
layout(set = 0, binding = 1) uniform sampler tex_sampler;

void main() {
    float b = texture(sampler2D(tex, tex_sampler), tex_coords).g;
    vec4 chem = vec4(0., 0., 0., 1.);
    vec4 background = vec4(0.39215686274509803, 0.14901960784313725, 0.6078431372549019, 1.);

    if (b > 0.15) {
        f_color = vec4(0., 0, 0., 1.);
    } else {
        f_color = vec4(0.3, 0., 0.4, 1.);
    }
}
"#;

pub const VERT_SHADER_SRC: &str = r#"
#version 450

layout(location = 0) in vec2 position;
layout(location = 0) out vec2 tex_coords;

void main() {
    gl_Position = vec4(position, 0.0, 1.0);
    tex_coords = (position + vec2(1.0)) * 0.5;
}
"#;
